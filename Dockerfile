FROM --platform=$BUILDPLATFORM golang:1.17.1-alpine3.14 as builder

ARG TARGETPLATFORM
ARG BUILDPLATFORM

WORKDIR /app
COPY go.mod /app/
COPY main.go /app/
COPY utils /app/utils
COPY build.sh /app/

RUN /app/build.sh

FROM --platform=$BUILDPLATFORM amd64/alpine as compresor

RUN apk add && \
	apk add upx

WORKDIR /app
COPY --from=builder /app/pantavisor-web-status /app/pantavisor-web-status
RUN if [ -n "$TARGETPLATFORM" ] && [ "$TARGETPLATFORM" != "linux/riscv64" ]; then upx pantavisor-web-status; fi

FROM scratch

WORKDIR /app

COPY --from=compresor /app/pantavisor-web-status /app/pantavisor-web-status
COPY static /app/static/
COPY templates /app/templates/
COPY --from=builder /lib/ld* /lib/
COPY --from=builder /lib/lib* /lib/

EXPOSE 5001

CMD [ "/app/pantavisor-web-status" ]
