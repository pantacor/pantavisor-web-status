package utils

import (
	"context"
	"fmt"
	"io"
	"net"
	"net/http"
)

// HttpUSC http unix socket with cache
type HttpUSC struct {
	*http.Client
	Cache map[string][]byte
}

type HttpUSCConfig struct {
	Socket string
}

func NewHttpUSC(config *HttpUSCConfig) *HttpUSC {
	if config == nil {
		config = &HttpUSCConfig{
			Socket: "/pantavisor/pv-ctrl",
		}
	}
	if config != nil && config.Socket == "" {
		config.Socket = "/pantavisor/pv-ctrl"
	}

	client := &HttpUSC{
		Cache: map[string][]byte{},
		Client: &http.Client{
			Transport: &http.Transport{
				DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
					return net.Dial("unix", config.Socket)
				},
			},
		},
	}

	return client
}

func (httpUsc *HttpUSC) GetCache(url string) ([]byte, error) {
	data, ok := httpUsc.Cache[url]
	go func() {
		data, err := httpUsc.Get(url)
		fmt.Println("Loading on cache ", url)
		if err == nil {
			httpUsc.Cache[url] = data
		}
	}()

	if !ok {
		return httpUsc.Get(url)
	}

	return data, nil
}

func (httpUsc *HttpUSC) Get(url string) ([]byte, error) {
	response, err := httpUsc.Client.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	contents, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	return contents, nil
}
