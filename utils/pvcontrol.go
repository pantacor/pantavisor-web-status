package utils

import (
	"encoding/json"
	"fmt"
)

// PvControl Client to the pv socket to manage a pantavisor device
type PvControl struct {
	Client *HttpUSC
}

type PVProgress struct {
	Status   string `json:"status"`
	Message  string `json:"status-msg"`
	Progress int    `json:"progress"`
}

type PVStep struct {
	Name      string     `json:"name"`
	CommitMsg string     `json:"commitmsg"`
	Progress  PVProgress `json:"progress"`
}

func NewPvControl(config *HttpUSCConfig) *PvControl {
	return &PvControl{
		Client: NewHttpUSC(config),
	}
}

func (pvctrl *PvControl) Preload(urls []string) {
	for _, url := range urls {
		go func(u string) {
			fmt.Println("Preloading ", u)
			pvctrl.Client.GetCache(u)
		}(url)
	}
}

func (pvctrl *PvControl) GetSteps() ([]PVStep, error) {
	response, err := pvctrl.Client.GetCache("http://localhost/steps")
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	steps := []PVStep{}
	if err = json.Unmarshal(response, &steps); err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	return steps, nil
}

func (pvctrl *PvControl) GetCurrentStep() (*PVStep, error) {
	steps, err := pvctrl.GetSteps()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	step := steps[0]
	for i := len(steps) - 1; i >= 0; i-- {
		if steps[i].Progress.Status == "DONE" || steps[i].Progress.Status == "UPDATED" {
			step = steps[i]
			break
		}
	}

	return &step, nil
}

func (pvctrl *PvControl) GetCurrentState() (string, error) {
	response, err := pvctrl.Client.GetCache("http://localhost/steps/current")
	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}

	return string(response), nil
}

func (pvctrl *PvControl) GetProgress(step string) (*PVProgress, error) {
	response, err := pvctrl.Client.GetCache("http://localhost/steps/" + step + "/progress")
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	progress := &PVProgress{}
	if err = json.Unmarshal(response, &progress); err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	return progress, nil
}

func (pvctrl *PvControl) GetDeviceMeta() (string, error) {
	response, err := pvctrl.Client.GetCache("http://localhost/device-meta")
	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}

	return string(response), nil
}
