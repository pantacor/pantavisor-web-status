package main

import (
	"context"
	"flag"
	"fmt"
	"html/template"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"sync/atomic"
	"syscall"
	"time"

	"gitlab.com/pantacor/pantavisor-web-status/utils"
)

type key int

const (
	requestIDKey key = 0
)

var (
	listenAddr string
	healthy    int32
)

type InnerJson map[string]string
type StateJson map[string]InnerJson

type Interface struct {
	net.Interface
	Addr []net.Addr
}

type IndexData struct {
	DeviceID   string
	Challenge  string
	GOARCH     string
	GOOS       string
	OS         string
	Host       string
	Kernel     string
	Machine    string
	Interfaces []Interface
	State      string
	Progress   *utils.PVProgress
	Step       *utils.PVStep
	DeviceMeta string
}

var pvcontrol *utils.PvControl

func main() {
	flag.StringVar(&listenAddr, "listen-addr", ":5001", "server listen address")
	flag.Parse()

	pvcontrol = utils.NewPvControl(nil)
	pvcontrol.Preload([]string{
		"http://localhost/steps",
		"http://localhost/steps/current",
		"http://localhost/steps/current/progress",
		"http://localhost/device-meta",
	})

	logger := log.New(os.Stdout, "http: ", log.LstdFlags)
	logger.Println("Server is starting...")

	router := http.NewServeMux()
	router.Handle("/", serveTemplate(logger))
	router.Handle("/healthz", healthz())
	router.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))

	nextRequestID := func() string {
		return fmt.Sprintf("%d", time.Now().UnixNano())
	}

	server := &http.Server{
		Addr:         listenAddr,
		Handler:      tracing(nextRequestID)(logging(logger)(router)),
		ErrorLog:     logger,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	go func() {
		<-quit
		logger.Println("Server is shutting down...")
		atomic.StoreInt32(&healthy, 0)

		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		server.SetKeepAlivesEnabled(false)
		if err := server.Shutdown(ctx); err != nil {
			logger.Fatalf("Could not gracefully shutdown the server: %v\n", err)
		}
		close(done)
	}()

	logger.Println("Server is ready to handle requests at", listenAddr)

	atomic.StoreInt32(&healthy, 1)

	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		logger.Fatalf("Could not listen on %s: %v\n", listenAddr, err)
	}

	<-done
	logger.Println("Server stopped")
}

func serveTemplate(logger *log.Logger) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		templatePath := filepath.Clean(r.URL.Path)

		if templatePath == "/" {
			templatePath = "/index.html"
		}

		lp := filepath.Join("templates", "layout.html")
		fp := filepath.Join("templates", templatePath)

		tmpl, _ := template.ParseFiles(lp, fp)
		data := getData(logger)

		err := tmpl.ExecuteTemplate(w, "layout", data)
		if err != nil {
			logger.Println(err.Error())
		}
	})
}

func getData(logger *log.Logger) *IndexData {

	deviceId, err := os.ReadFile("/pantavisor/device-id")
	if err != nil {
		logger.Println(err.Error())
		deviceId = []byte("Not Claimed")
	}

	phHost, err := os.ReadFile("/pantavisor/pantahub-host")
	if err != nil {
		logger.Println(err.Error())
	}

	challenge, err := os.ReadFile("/pantavisor/challenge")
	if err != nil {
		logger.Println(err.Error())
	}

	stateString, err := pvcontrol.GetCurrentState()
	if err != nil {
		logger.Println(err.Error())
	}

	step, err := pvcontrol.GetCurrentStep()
	if err != nil {
		logger.Println(err.Error())
	}

	progress, err := pvcontrol.GetProgress("current")
	if err != nil {
		logger.Println(err.Error())
	}

	deviceMeta, err := pvcontrol.GetDeviceMeta()
	if err != nil {
		logger.Println(err.Error())
	}

	ainterfaces, _ := net.Interfaces()
	var interfaces []Interface

	for _, val := range ainterfaces {
		if val.HardwareAddr != nil {
			i := Interface{}
			i.Interface = val
			i.Addr, _ = val.Addrs()
			interfaces = append(interfaces, i)
		}
	}
	uname := &syscall.Utsname{}

	data := &IndexData{
		DeviceID:   string(deviceId),
		Challenge:  string(challenge),
		Host:       string(phHost),
		GOARCH:     runtime.GOARCH,
		GOOS:       runtime.GOOS,
		OS:         "",
		Kernel:     "",
		Machine:    "",
		Interfaces: interfaces,
		State:      stateString,
		Progress:   progress,
		Step:       step,
		DeviceMeta: deviceMeta,
	}
	if err := syscall.Uname(uname); err == nil {
		data.OS = int8ToStr(uname.Sysname[:])
		data.Kernel = int8ToStr(uname.Release[:])
		data.Machine = int8ToStr(uname.Machine[:])
	}

	return data
}

func healthz() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if atomic.LoadInt32(&healthy) == 1 {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusServiceUnavailable)
	})
}

func logging(logger *log.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				requestID, ok := r.Context().Value(requestIDKey).(string)
				if !ok {
					requestID = "unknown"
				}
				logger.Println(requestID, r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent())
			}()
			next.ServeHTTP(w, r)
		})
	}
}

func tracing(nextRequestID func() string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			requestID := r.Header.Get("X-Request-Id")
			if requestID == "" {
				requestID = nextRequestID()
			}
			ctx := context.WithValue(r.Context(), requestIDKey, requestID)
			w.Header().Set("X-Request-Id", requestID)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

// A utility to convert the values to proper strings.
func int8ToStr(arr interface{}) string {
	var b []byte

	switch arr := arr.(type) {
	case []int8:
		b = make([]byte, 0, len(arr))
		for _, v := range arr {
			if v == 0x00 {
				break
			}
			b = append(b, byte(v))
		}
	case []uint8:
		b = make([]byte, 0, len(arr))
		for _, v := range arr {
			if v == 0x00 {
				break
			}
			b = append(b, byte(v))
		}
	}

	return string(b)
}
